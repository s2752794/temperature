package nl.utwente.di.ctoFConverter;

public class Converter {
    public double convert(String temp) {
        double t=Double.parseDouble(temp);
        return (t*9/5)+32;
    }
}
